function distance(v1,v2){
	
		if(!Array.isArray(v1) || !Array.isArray(v2)) throw new Error("InvalidType");
	else if(v1.length==0 || v2.length==0) return 0;
		else{
			
			const unique = (value, index, self) => {
				return self.indexOf(value) === index
			}
			
		var distanta=0;
		var i,j;
		var vect_comun=null;
		
		var v1_dist=v1.filter(unique);
		var v2_dist=v2.filter(unique);
		
		for( i=0;i<v1_dist.length;i++)
				for( j=0;j<v2_dist.length;j++)
					if(v1_dist[i]==v2_dist[j] && typeof(v1_dist[i])==typeof(v2_dist[j])) {if(vect_comun==null) vect_comun=(v1_dist[i]);
																						else if(!vect_comun.includes(v1_dist[i])) vect_comun.push(v1_dist[i]);}
					                              
		
		var elem_comun;
		if(vect_comun==null) elem_comun=0;
		else elem_comun=vect_comun.length;
			
		distanta=v1_dist.length+v2_dist.length-2*elem_comun;	
			
		return distanta;
	}
	}



module.exports.distance = distance